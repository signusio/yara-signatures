rule eth_wallet_v3
{
    meta:
        author = "signus"
        desc = "Ethereum wallet v3 keystore"
        version = "v0.1"
    strings:
        $a = "\"address\":" ascii nocase
        $b = "\"crypto\":" ascii nocase
        $c = "\"cipher\":" ascii nocase
        $d = "\"kdfparams\":" ascii nocase
        $e = "\"dklen\":" ascii nocase
        $f = "\"salt\":" ascii nocase
        $g = "\"mac\":" ascii nocase
        $h = "\"id\":" ascii nocase
        $i = "\"version\":" ascii nocase
    condition:
        all of them
}
